# Services : Chrony role

The purpose of this role is to install, configure and enable the chrony service.
This role is part of the [STARTX services ansible collection](https://galaxy.ansible.com/startxfr/services).

## Requirements

- Ansible runtime
- Installation of the [startx services collection](https://galaxy.ansible.com/startxfr/services) with `ansible-galaxy collection install startxfr.services`

## Role Variables

| Key                   | Default          | Description               |
| --------------------- | ---------------- | ------------------------- |
| ss_chrony_action      | create           | The action to perform     |
| ss_chrony_release     | latest           | chrony version to install |
| ss_chrony_config_file | /etc/chrony.conf | chrony configuration file |

## Dependencies

Depend only on `ansible.builtin`

## Example playbooks

### Install chrony playbook

Install chrony service default version.

```yaml
- name: Install chrony service
  hosts: localhost
  roles:
    - role: startxfr.services.chrony
```

### Install chrony version 4.2-1" playbook

Install chrony service 4.2-1" version.

```yaml
- name: Install chrony service in version 4.2-1"
  hosts: localhost
  roles:
    - role: startxfr.services.chrony
      ss_chrony_release: "4.2-1"
```

### Uninstall chrony playbook

Uninstall chrony service.

```yaml
- name: Uninstall chrony service
  hosts: localhost
  roles:
    - role: startxfr.services.chrony
      ss_chrony_action: "delete"
```
