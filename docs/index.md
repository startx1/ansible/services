# STARTX services Ansible collection

This is the main documentation for the `startxfr.services` ansible collection.

## Roles

| version                        | Description                         |
| ------------------------------ | ----------------------------------- |
| [chrony role](roles/chrony/)   | Install or uninstall chrony service |
| [apache role](roles/apache/)   | Install or uninstall httpd service  |
| [mariadb role](roles/mariadb/) | Install or uninstall mysqld service |
| [vault role](roles/vault/)     | Install or uninstall vault service  |

## History

If you want to follow the collection history, you can follow the [release history](history.md).

## Improve and develop role

If you want to contrbute to the startx inititive you can follow the [developper guidelines](developpers.md).

## Contributors

A full list of the contributors is available under the [contributors list page](contributors.md).
