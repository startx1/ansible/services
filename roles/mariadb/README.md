# STARTX services : MariaDB role

Read the [startxfr.services.mariadb role documentation](https://startx-ansible-services.readthedocs.io/en/latest/roles/mariadb/)
for more information on how to use [STARTX services ansible collection](https://galaxy.ansible.com/startxfr/services) mariadb role.
