# STARTX services : Chronyd role

Read the [startxfr.services.chrony role documentation](https://startx-ansible-services.readthedocs.io/en/latest/roles/chrony/)
for more information on how to use [STARTX services ansible collection](https://galaxy.ansible.com/startxfr/services) chrony role.
