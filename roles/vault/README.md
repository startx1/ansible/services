# STARTX services : Vault role

Read the [startxfr.services.vault role documentation](https://startx-ansible-services.readthedocs.io/en/latest/roles/vault/)
for more information on how to use [STARTX services ansible collection](https://galaxy.ansible.com/startxfr/services) vault role.
